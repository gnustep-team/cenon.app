#! /bin/sh
# Cenon - built Release (XCode 4 sucks)
# created:  2014-01-04
# modified: 2014-02-24 (make clean before compiling)

SCRIPT_DIR=${0%/*}	# $0 == $BASH_SOURCE
[[ "${SCRIPT_DIR:0:1}" != "/"  ]] && { cd $SCRIPT_DIR; SCRIPT_DIR=`pwd`; }
cd $SCRIPT_DIR

xcodebuild -configuration Release -project Cenon.xcodeproj clean

xcodebuild -configuration Release -project Cenon.xcodeproj

exit 0
