Cenon Change Log - 2014

2014-01-04 Georg Fleischmann
    * vhf2DFunctions.m (vhfIsPointInsidePolygon()): free(iPts)

2014-01-05 Georg Fleischmann
    * DocView.m (-redrawObject:, -moveObject:, -rotateObject:): copy cache image (avoid gstate)

Cenon 4.0.2 (2014-01-05)

2013-01-13 Ilonka Fleischmann (2012-10-31)
	* Cenon/ProjectSettings.m (-documentHasChanged:): App preferencesPanel update:self for optimize inside switch

2014-01-14 Georg Fleischmann
    * PreferencesPanel.m (): prevent exception, if module fails to load

Cenon 4.0.2 (2014-01-15)

2014-01-27 Georg Fleischmann
    * DocView.m (-draw:): [cacheView gState] is returning 0 if there is a text on a layer
        -> Fix, graphics cache in 4.0.2 (2014-01-15) with Mac OS < 10.9:
                cache is now working again with layers mixed with and without text
    * DocView.m (-draw:): release bitmap
        -> Fix, memory leak in 4.0.2 (2014-01-15)
    * vhfCompatibility.h: NSAppKitVersionNumber10_9 = 1265 added

Cenon 4.0.2 (2014-01-27)


2014-02-13 Ilonka Fleischmann
	* IntersectionPanel (-create:): check if position is allready used, only one for each pt
	  -> Fix, Intersection Panel: create only one graphics for many intersection at one point

2014-02-18 Georg Fleischmann
    * DocView.m (-drawRect:): "else if (cacheView)" only, no check for gState)
      -> in case gstate is completely random
    * VPath.m (-drawWithPrincipal): not stroked + ! antialiased was invisible
      -> Fix, Path drawing: stroked paths without antialias are visible again [since 4.0.2]

Cenon 4.0.2 (2014-02-18)

2014-02-24 Georg Fleischmann
    * XCode Project: use 10.6 SDK for 32 Bit, 10.5 SDK will crash in [NSImage -drawInRect:]
      -> Fix, 32 Bit works again

Cenon 4.0.2 (2014-02-24)

2014-02-24 Georg Fleischmann
    * UpdateController.m (-checkForUpdate:): if version is equal, check date from "version"
      -> Fix, Update-Panel: if version is the same, check for newer date

2014-02-28 Georg Fleischmann
    * VImage.m (-drawWithPrincipal:): don't scale bounds.size with drawAtPoint: (2 locations changed) !
      -> Fix, Image: Zoom of scaled images works again

Cenon 4.0.2 (2014-02-28)


2014-04-14 Georg Fleischmann
    * SVGImport.m (-parser:foundCharacters:): [stringFound retain] added
      -> Fix, SVG-Import: crash fixed when importing text

2014-06-14 Georg Fleischmann
    * Document.m (+newFromFile:): load "scaleFactor" as VFloat
      DocView.m (-scaleUnitSquareToSize:):  save "scaleFactor" as VFloat
      DocView.m (-scaleSize, scaleSize): new
      TileScrollView.m (-scaleTo:, -scale:):  float -> VFloat
      propertyList.m, h (propertyListFromVFloat(), vfloatFromPropertyList()): new
      -> More changes to arrive at full 64 bit compatibility
2014-06-14 Georg Fleischmann
    * DocView.m (-scaleSize, scaleSize): new
      -> steps towards width != height scaling

2014-06-16 Georg Fleischmann
    * Info.plist: NSHighResolutionCapable = YES added
      -> enables High Resolution mode ?
      
Cenon 4.0.3 beta 1 (2014-06-28)

2014-07-05 Georg Fleischmann (thanks to: Yavor Doganov <yavor@gnu.org>)
    * PSImport.m (-importPDFFromFile:, -importPS:):
      -> Fix, PS-Import: alert user, if no ghostscript is installed
    * TileScrollView.h: message signature NSPopUpButton added to resPopupListButton
    * VCurveFit.m (-fitGraphic:): *pts = NULL (not nil)
    * VGroup.h (-setList:): mutable array, same as in code
    * IPAllAcc.h (accPopup): message signature NSPopUpButton added
      IPAllFilling.h (fillPopup): message signature NSPopUpButton added
      IPAllStrokeWidth.h (strokePopup): message signature NSPopUpButton added
      IPImage.h (compPopup): message signature NSPopUpButton added
    * DINImportSub.m (sortLayer()): NSComparisonResult instead of NSInteger (should be the same anyway)
    * SVGImportSub.m (-addGroupList:..., -allocateList): message signature for aList mutable
    * DXFImport.m (--getText:):  fontName added to internal declaration
    
2014-07-06 Georg Fleischmann
    * InspectorPanel.m, h: VGraphic* instead of id
      InspectorPanel.m (-loadGraphic:): message signature added for tpGraphics, tpWindow, tpView
    * PreferencesPanel.h (moduleView): message signature NSBOx added
    * VHFPopUpButtonAdditions.m (-selectItemWithTag:): removed, since it is available since OS 10.4
    * IPLine3D.m: [(NSButton*)sender tag]
    * VText.m, h (-initWithFile:): dummy added
    * Type1Font.m: 4 random bytes with legal characters
    * SVGImport.h: @interface SVGImport:NSObject <NSXMLParserDelegate>

2014-07-11 Georg Fleischmann
    * VHFStringAddirions.m, h (-componentsSeparatedByCharactersInString:): new

2014-07-15 Georg Fleischmann
    * Document.xib: Scale of 125% added

2014-07-22
    * VPolyLine.m (-initFromPropertyList:): check for successful scan to handle nan entries

2014-08-29 Ilonka Fleischmann
	* DXFImport.m (-scanGroup:): group.color no init
	  -> Fix, DXF-Import: Pass color down the graphics hierarchy
	* DXFImport.m (-getArc::::): if group.extX-Z transform the complete arc not only the ctr
	  -> Fix, DXF-Import: corrected Arcs with extrusion direction

2014-10-06 Georg Fleischmann
    * App-Id: de.vhf.Cenon -> com.Cenon

2014-08-29 Ilonka Fleischmann
	* VCurve.m,h (-gradientAt:, tileCurveAt(), -splittedObjectsAt:, pointOnCurveNextToPoint(),
	              -getListOfObjectsSplittedFromGraphic:, -getListOfObjectsSplittedFrom:):
	  -> Fix VFloats Curve Contour Calculations: 64 Bit

2014-10-13 (2014-09-11) Ilonka Fleischmann
	* VCurve.m,h (-gradientAt:): handle p2,p3 in start/end point better
	  VCurve.m,h (intersectCurves()): 15.0 instead 20.0 * TOLERANCE
	  -> Fix, Curve: calculations of curves with only 3 points improved

2014-10-13 (2014-02-06) Ilonka Fleischmann
	* VArcOutput.m (-createOutput:tools:): use real start of full arc, instead at 0 degree
	  VArc.m (-contour:): full arc contour have now the same startangle, not allways 0 degree
	* VArc.m (-tangentIntersectionCurve:::, distancePointCurveIsRadius()): double instead float
	  -> Fix Full Arc Output: Calculated with real startpoint, not allways at 0 degree
2014-10-13 (2014-08-29) Ilonka Fleischmann
	* VArc.m (-clippedWithRect:): for NSContainsRect we use the coordBounds instead the bounds
	  -> Fix, Arc Clipping: arc sections completele inside Clipping Rectangle improved

2014-10-14 (2014-07-23) Ilonka Fleischmann
	* App.m (-import:): .cenon added
	  Document.m, h (DOCUMENT_NAME, OUTPUT_NAME): moved from .m to .h
	  -> New, Import: import of Cenon files ".cenon" added (Menu > File > Import)

2014-10-14 (2014-07-26) Ilonka Fleischmann
	* VPolyLine.m (-movePoint:by:): start/end points moved both if near enough
	  -> Fix, PolyLine: moving of start/end points improved

2014-10-14 (2014-09-02) Ilonka Fleischmann
	* VGroup.m (-setDirectionCCW: -changeDirection): new
	* VText.m (-setDirectionCCW:, -getFlattenedObjectAt:..): new; set Direction to created group/path
	  VTextPath.m (-getFlattenedObjectAt:..): set Direction to created group/path
	* VPolyLine.m (-setDirectionCCW:, orthPointAt()): corrected ! , new
	  -> Fix, Output: direction of groups, text, PolyLine in output path corrected

2014-10-14 (2014-08-29) Ilonka Fleischmann
	* VPath.m (-drawWithPrincipal:): showDirection , not for realy short lines inside path 
	  -> Fix, Draw Path Direction: very small objects inside Path (but startpoints) will not shown

2014-10-14 (2014-10-05) Ilonka Fleischmann
	* VPath.m,h (-contour:inlay:splitCurves:): remove short graphics, replace with one line if more than tolerance length
	  VPath.m,h (-subPathInsidePath::): if on,  no k++, so we jump over one (last) graphic
	  VPath.m (-optimizeSubPathsToClosedPath:): compare possibleS with possibleE index diff > 2 instead > 1
	  VPath.m (-optimizeSubPathsToClosedPath:): a lot more, like bigger tol for first part
	  VPath.m (-getLastObjectOfSubPath:tolerance:): needed for -contour:inlay:splitCurves because we remove short graphics
	  -> Fix Path Contour/PickOut Filling: a lot better again
	  VPath.m (-contour:inlay:splitCurves:, orthPointAtBegOrEnd(), orthPointAt(), ):
	  -> Fix, VFloats Path Contour Calculations: 64 Bit

2014-10-14 (2014-10-09) Ilonka Fleischmann
    * VPath.m, h (-contourOpenLeftOrRight:): new

2014-10-14 Georg Fleischmann
    * UpdateController.m (-checkForUpdates:): date-string from defaults is now NSDate
      -> Fix, Updates: updatechecks with auto-check didn't work since 24.02.2014 (4.0.3 p1)

2014-10-16 Georg Fleischmann
    * App.m (+initialize): copy our defaults from "de.vhf.Cenon" -> "com.Cenon"
	  VPath.m,h (-contourOpenLeftOrRight:): new

2014-11-01 Georg Fleischmann
    * CreateGraphicsChange.m    (-redoChange): loadGraphic: -> loadList:
      MoveLayerGraphicsChange.m (-redoChange): loadGraphic: -> loadList:
    * English.lproj/Localizable.strings: header added and saved with UTF-8 encoding
    * XCode-Project: use 10.8 SDK for building (except ppc)
      Note: Cenon with XCode 6.1 on Yosemite only works with SDK = 10.9 !!

2014-11-02 Cenon 4.0.3 b4 pre-release

2014-11-03 (2014-10-09) Ilonka Fleischmann
	* VArc.m,h (-setDirectionCCW:): only if not filled or angle < 360
	  VArc.m,h      (-contourOpenLeftOrRight:): new
	  VLine.m,h     (-contourOpenLeftOrRight:): new
	  VCurve.m,h    (-contourOpenLeftOrRight:): new
	  VPolyline.m,h (-contourOpenLeftOrRight:): new

2014-11-04 (2014-10-21) Ilonka Fleischmann
	* TilePanel.m, h (-setDistancePopup:, -updatePanel:, -setXYDirections): tileXYDirections new
	  dvTile.m (-setTileWithLimits:,-buildTileCopies:, …): tileXYDirections new
	  DocView.m, h (-initWithCoder:, -initFromPropertyList:, -propertyList, -initWithCoder:): tileXYDirections new
	  *.lproj/TilePanel.xib (alle 4): Arrow Buttons added
	  -> Batch: more intuitive and flexible (with directional arrows)

2014-11-04 Georg Fleischmann
    * VHFStringAdditions.m (+stringWithContentsOfFile:): use UTF8Encoding if determination of encoding fails

2014-11-07 Georg Fleischmann
    * VHFSystemAdditions.m, h (-createDirectoryAtPath: recursive: attributes: error:): new, our own version
                              (-removeFileAtPath: error:): new, our own version
                              (-movePath: toPath: error:): new, our own version
                              (-copyPath: toPath: error:): new, our own version
                              (-directoryContentsAtPath:error:): new, our own version of old method
                              (-contentsOfDirectoryAtPath:): new, our own version of new method (without "error:")
      Document.m (-save): call [NSFileManager -createDirectoryAtPath:recursive:attributes:error:]
      UpdateController.m (-tableViewSelectionDidChange:): call [NSFileManager -createDirectoryAtPath:recursive:attributes:error:]
      App.m (-loadModules): call [NSFileManager -contentsOfDirectoryAtPath:]
      PreferencesPanel.m (-loadModules): call [NSFileManager -contentsOfDirectoryAtPath:]
      functions.m (fillPopup()): call [NSFileManager -contentsOfDirectoryAtPath:]

2014-11-07 Cenon 4.0.3 b6 pre-release

2014-11-09 Georg Fleischmann
    * App.h: @interface App : NSApplication <NSApplicationDelegate>
    * App.m (-applicationDidFinishLaunching, -takeSnapshot): call [NSFileManager -createDirectoryAtPath:recursive:attributes:error:]

2014-11-10 Cenon 4.0.3 b7 pre-release

2014-11-13 Georg Fleischmann
    * Info-VHFImport.plist: CFBundleExecutable = VHFImport
      Info-VHFExport.plist: CFBundleExecutable = VHFExport to make it codesign on Yosemite

2014-11-13 Cenon 4.0.3 b9 pre-release

2014-11-13 Georg Fleischmann
    * Info-VHFImport -> Info-CenImport
      Info-VHFExport -> Info-CenExport

2014-11-20 Cenon 4.0.3 b10 pre-release
...
2014-12-08 Cenon 4.0.3 b13 pre-release

2014-12-09 Ilonka Fleischmann
	* DocView.m (-initialize:): version 8
	  DocView.m (-initFromPropertyList:): versions < 8 correct Values to new batch
	  -> Fix, Batch: old documents from Cenon 4.0.2 are handled correctly

2014-12-11 Cenon 4.0.3 b14 pre-release

2014-12-18 Georg Fleischmann
    * PathContour.m (-contourImage:width:): unlockFocus, close window right after drawing
    * PathContour.m (-contourImage:width:): scale bitmapImage down to size, if necessary (Mac OS 10.10)
      -> Fix, Vectorizer: workaround for vectorizing with Mac OS 10.10 (and Retina) to avoid crash

2014-12-19 Georg Fleischmann
    * VCurveFit.m (GenerateBezier()): caught division by zero: if (det_C0_C1 == 0.0)
    * PathContour.m (-contourImage:): all (LONG*) removed (array is only "unsigned char" and "long" gets 4 chars)

next Cenon 4.0.3 (2014-12-19)
