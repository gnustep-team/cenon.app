Cenon 4.0.6 (2017-07-04)

+ Fix, color shades: possible division by zero fixed
+ Fix, DXF-Export: export of closed Paths corrected [since 4.0.4]
+ Fix, Text Path: handle return character within text
+ Fix, Transform-Panel: scale absolute for 0-sized objects
+ Fix, Punch: issue in certain situations fixed
+ Fix, Punch: works for rectangles in certain difficult arrangements [beta 2]
+ Fix, Sierra: workaround for saving TIFF images

Cenon 4.0.5 (2016-11-21)

+ Fix, Calculus: potential issue (division by zero)
+ Fix, Contour: Crash in very complex paths fixed
+ Fix, Path Contour: improved
+ Fix, Rectangle: copied rectangle includes direction
+ DXF-Import: improved (skip SPLINE frame control point in POLYLINE)
beta 2:
+ Fix, Contour left or right: with inside correction we move against milling direction [since 4.0.5 beta 1 ?]
+ Fix, Set Start Point: start point can not be set for open paths
+ Fix, Calculus: potential issue fixed (division by zero)
beta 3:
+ Mac OS 10.12, Sierra
+ Fix, Batch: 4.0.3 documents open with default-arrows pointing right/up [since 4.0.3]
+ Fix, Inlays: improved
+ Fix, Contour + Filling: again some bugs less
+ Fix, Output calculation: adding/removing a point to/from Path or Polyline now recalculates
+ Fix, Document-Window: caching issue fixed, which could result in a crash when closing a document
beta 4:
+ Fix, Text-Path: resize via Inspector works again [since 4.0.0]

Cenon 4.0.4 (2016-03-14)

+ Tool-Panel: German localization (Tool-Tips) added
+ Fix, DXF-Import: formatting code removed from string (AutoCAD >= 13)
+ Fix, DXF-Export: export of open Paths with multiple sub-paths
+ Fix, Gerber-Export: select tool for path elements
+ Fix, Groups: drag select with text or images without logging to console
+ Fix, Mac OS X 10.10: restrict document cache to a size Yosemite / Retina displays can handle
+ Fix, Mac OS X 10.10: Zoom (+/-) works now with Yosemite
+ Fix, Open File: catch console warning of open/save panel
+ Fix, Batch: New Documents open with Arrows pointing right and up
+ Fix, Add Point: Undo with multiple selected objects improved
+ Fix, Rotating Arcs with tool improved
+ Fix, Undo of Drag Point improved
+ Internal: Source prepared for SDK 10.10 issues (display functions redesigned)
beta 3:
+ SVG-Import: quadratic bezier curve implemented (q, Q, t, T)
+ Fix, SVGImport: stroked rect closed
+ Fix, Contour Left/Right: improved
beta 4:
+ New, Import: Preferences setting (Import -> Move to Origin) to move imported files to origin or not
+ PS-Import: improved to handle insane EPS files by pre-converting to simpler EPS
+ Printing: paper margins set to Null (stupid Apple redundancy since Mac OS 10.8)
+ Fix, Raster Contour, Mac OS 10.10: Retina/Yosemite workaround for large working areas
+ Fix, Contour Left/Right for Arc: improved (should have been here since 2015-08-11)
beta 5
+ Import: document gets the name of the imported file (not UNTITLED)
+ Fix, DXF-Import: certain Arcs with 180 degree angle or extrusion direction were inverted (extrusion Z = -1)
+ Export: extensions with three letters added:
          "cenon",  "cen"
          "gerber", "ger"
          "hpgl",   "hgl", "plt"
          "tiff",   "tif"
+ Fix, Text: selecting (by click) rotated text improved
+ Fix, Undo: change length Undo (Inspector)
+ Fix, Mac Os 10.10 (Yosemite): import directory is preserved for save
+ Fix, Mac OS 10.10 (Yosemite): double clicking title bar of document window will fold up window (not full screen mode)
+ Fix, Mac OS 10.11 (El Capitan): open document works
beta 6:
+ Fix, Mirrored Text: after resize, mirrored text could turn invisible (Mac OS 10.8 - 10.10)
+ Fix, Rotated Text: locked rotated text is not moving any more after editing
+ Fix, Undo: Undo for Fill object
+ Fix, Contour: possible crash fixed for Curves with zero length (or control points equal end points)
final:
+ Fix, Paste: no paste without editable layer
+ Fix, Paste: potential crash in certain situations


Cenon 4.0.3 (2015-02-09)

+ New, Scale of 125% added
+ New, Import: Import of Cenon files ".cenon" added (Menu > File > Import)
+ New, Batch: more intuitive and flexible (with directional arrows)
+ Convenience: link from HOME/Documents/Cenon -> HOME/Library/Application Support/Cenon
+ Fix, DXF-Import: Pass color down the graphics hierarchy
+ Fix, DXF-Import: corrected Arcs with extrusion direction
+ Fix, Intersection Panel: create only one graphics for many intersection at one point
+ Fix, Update-Panel: same version, later date works now
+ Fix, SVG-Import: crash fixed when importing text
+ Fix, Image: Zoom of scaled images works again [4.0.2, 2014-02-28]
+ Fix, Curve: calculations of curves with only 3 points improved
+ Fix, Arc Clipping: arc sections completely inside Clipping Rectangle improved
+ Fix, PolyLine: moving of start/end points improved
+ Fix, Vectorizer: vectorizing on Yosemite (OS 10.10) repaired

Cenon 4.0.2 (2014-02-24)

+ Fix, 4.0.2: Zoom of scaled images works again [2014-02-28]
+ Fix, 4.0.2: 32 Bit works [2014-02-24]
+ Fix, 4.0.2: stroked paths without Antialiasing are visible again [2014-02-18]
+ Zoom with Command + / -
+ Grid: the Menu Format->Grid has assigned new key shortcuts:
  Cmd-* (On/Off), CMD-Alt-* (Panel), and Cmd-Shift-* (Align to Grid)
+ Transparency in Color-Panel
+ DXF-Import: import font of text if available
+ New: Opening a document restores scale factor
+ Fix, 64 Bit: 64 Bit improvements
+ Fix, Printing: set minimum line width to Preferences setting
+ Fix, AI-Import: more tolerant handling of string encodings
+ Fix, Arc Contour: accuracy improved
+ Fix, Rectangle: selection of rectangles with round corners improved
+ Fix, DXF-Import: place inserts on correct layer
+ Fix, DXF-Export, Apple: workaround for special characters in layer names
+ Fix, Move graphics: grabbing of selected graphics at control point is possible now
+ Fix, Rotate Object(s): Improvements
+ Fix, GNUstep: some fixes to make Cenon compile on Windows (thank's to Jason Whiteman)
+ Fix, Apple: Mavericks (OS 10.9): we rewrote the graphics cache of Cenon to work with Mavericks (OS 10.9)

Cenon 4.0.1 (2012-11-16)

+ Code-Signing for GateKeeper / Mountain Lion added
+ Package Requirements: >= Mac OS 10.5 (Leopard), Intel Processor
+ Snapshots: close documents before opening snapshot
+ Fix, Drag-Select: selection of groups improved

Cenon 4.0.0 (2012-07-19)

+ New: Import of Scalable Vector Graphics (SVG)
       Millions of vector data are readily available on the Internet,
       Signs, Logos, Crests, Symbols, Icons and more are waiting for
       download.
+ New: Vectorizing Images. A new menu entry "Tools -> Vectorize Images"
       shows a new Vectorising Panel.
       Vectorization of Images to lines and curves is supported.
+ New, Apple: 64 bit binary for Intel processors added
       3-way universal binary (Intel 64 bit, Intel 32 bit, PPC 32 bit)
+ New, Transform-Panel: scale absolute added
+ New, Set starting point of a selected Path
       1. select a point of a Path
       2. from the menu chose "Edit -> Set Start"
+ New in Open-Panel: Buttons to jump to Cenon locations:
       Cenon-Library, Cenon User-Library, Cenon Documents
+ New in Intersection-Panel: creation of objects from Marks, Threads, Sinkings, Webs added
+ Batch: sort columns more effective, 1. upwards, 2. downwards, 3. upwards, ...
+ Build Contour: created graphic will be selected
+ ToolPanel: Tool-Tips added, icons that are not needed are removed
+ Edit-Menu, Tool-Menu: Tool Tips added
+ Import DXF: import 3D Lines and 3D Faces (so far as lines only)
+ Import PDF/PS: Alert if GhostScript is not installed
+ Import PS: PostScript Operators for character displacement added (xshow, yshow, xyshow).
  When keeping texts editable (no flatten text), the displacement will be ignored.
+ Grid: every tenth line is drawn thicker for better overview
+ Select Object: no annoying beep when locked object is being selected
+ Text: editing a locked Text will not adjust the size of the text box any more
+ Working Area Panel: changes are accepted by pressing Tab key (as well as Enter)
+ Editing, Snap: address issue where snap to point skips some points
+ Inspector: improve setting of angles
+ Layer-Inspector: Fast switching on and off of a layer
  does not open the Layer-Details Panel any more.
+ Apple, Text: Fit Horizontal text improved
