/* Type1ImportSub.m
 * Subclass of Type1 import for creation of graphic objects
 *
 * Copyright (C) 1996-2014 by Cenon GmbH
 * Author:  Georg Fleischmann
 *
 * created:  1996-02-09
 * modified: 2002-07-15
 *
 * This file is part of the Cenon Import Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by the Cenon GmbH. Among other things,
 * the License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this library; see the file LICENSE. If not, write to Cenon.
 *
 * If you want to link this library to your proprietary software,
 * or for other uses which are not covered by the definitions
 * laid down in the Cenon Public License, vhf also offers a proprietary
 * license scheme. See the vhf internet pages or ask for details.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_TYPE1IMPORTSUB
#define CEN_H_TYPE1IMPORTSUB

#include <AppKit/AppKit.h>
#include <Type1Import.h>

@interface Type1ImportSub:Type1Import
{
//    Font	fontObject;
}

/* subclassed from super class
 */
- allocateFontObject;
- allocateList;
- (void)addLine:(NSPoint)beg :(NSPoint)end toList:aList;
- (void)addArc:(NSPoint)center :(NSPoint)start :(float)angle toList:aList;
- (void)addCurve:(NSPoint)p0 :(NSPoint)p1 :(NSPoint)p2 :(NSPoint)p3 toList:aList;
- (void)addStrokeList:(NSArray*)aList toList:(NSMutableArray*)bList;
- (void)addFillList:(NSArray*)aList   toList:(NSMutableArray*)bList;
- (void)addText:(NSString*)text :(NSString*)font :(float)angle :(float)size :(float)ar at:(NSPoint)p toList:aList;
- (void)setBounds:(NSRect)bounds;

@end

#endif // CEN_H_TYPE1IMPORTSUB
