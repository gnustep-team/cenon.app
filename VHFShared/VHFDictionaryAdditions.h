/* VHFDictionaryAdditions.h
 * NSDictionary / NSMutableDictionary additions
 *
 * Copyright (C) 1997-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  1997-07-08
 * modified: 2013-12-19 (-VFLoatForKey:)
 *           2010-05-07 (-boolForKey:)
 *           2009-04-18 (new: -v3Point:ForKey:, -setV3Point:forKey:)
 *
 * This file is part of the Cenon Shared Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by the Cenon GmbH. Among other things,
 * the License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this library; see the file LICENSE. If not, write to Cenon.
 *
 * If you want to link this library to your proprietary software,
 * or for other uses which are not covered by the definitions
 * laid down in the Cenon Public License, vhf also offers a proprietary
 * license scheme. See the vhf internet pages or ask for details.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.cenon.info
 */

#ifndef CEN_H_DICTIONARYADDITIONS
#define CEN_H_DICTIONARYADDITIONS

#include <Foundation/Foundation.h>
#include "types.h"  // V3Point

@interface NSDictionary(VHFDictionaryAdditions)
- (NSString*)stringForKey:(id)key;
- (NSNumber*)numberForKey:(id)key;
- (BOOL)boolForKey:(id)key;
- (int)intForKey:(id)key;
- (float)floatForKey:(id)key;
- (double)doubleForKey:(id)key;
- (VFloat)VFloatForKey:(id)key; // 32: float, 64: double
- (BOOL)containsPrefix:(NSString*)prefix;

- (V3Point)v3PointForKey:(id)key;
@end

@interface NSMutableDictionary(VHFDictionaryAdditions)
- (void)setInt:(int)i forKey:(id)key;
- (void)setFloat:(float)f forKey:(id)key;
- (void)setDouble:(double)d forKey:(id)key;

- (void)setV3Point:(V3Point)pt forKey:(id)key;
@end

#endif // CEN_H_DICTIONARYADDITIONS
