/*
 * VHFSystemAdditions.h
 *
 * Copyright (C) 2000-2015 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  2000-04-30
 * modified: 2014-11-07 (NSFileManager Additions added)
 *
 * This file is part of the Cenon Shared Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by the Cenon GmbH. Among other things,
 * the License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this library; see the file LICENSE. If not, write to Cenon.
 *
 * If you want to link this library to your proprietary software,
 * or for other uses which are not covered by the definitions
 * laid down in the Cenon Public License, vhf also offers a proprietary
 * license scheme. See the vhf internet pages or ask for details.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_SYSTEMADDITIONS
#define CEN_H_SYSTEMADDITIONS

#include <AppKit/AppKit.h>

@interface NSBundle(VHFBundleAdditions)
+ (BOOL)loadModelNamed:(NSString*)fileName owner:(id)owner;
@end

@interface NSFileManager(CenFileManagerAdditions)
- (BOOL)createDirectoryAtPath:(NSString*)path recursive:(BOOL)recursive                     // our version of new method
                   attributes:(NSDictionary*)attributes error:(NSError **)error;
- (BOOL)removeFileAtPath:(NSString*)path error:(NSError **)error;                           // our version
- (BOOL)movePath:(NSString *)srcPath toPath:(NSString *)dstPath error:(NSError **)error;    // our version
- (BOOL)copyPath:(NSString *)srcPath toPath:(NSString *)dstPath error:(NSError **)error;    // our version
- (NSArray*)directoryContentsAtPath:(NSString *)path error:(NSError **)error;               // our version of old method
- (NSArray*)contentsOfDirectoryAtPath:(NSString *)path;                                     // our version of new method
- (NSDictionary*)fileAttributesAtPath:(NSString*)path traverseLink:(BOOL)traverse           // our version of old method
                                error:(NSError **)error;
@end

#endif // CEN_H_SYSTEMADDITIONS
