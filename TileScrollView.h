/*
 * TileScrollView.h
 *
 * Copyright (C) 1996-2014 by Cenon GmbH
 * Author: Georg Fleischmann
 *
 * Created:  1993
 * Modified: 2014-06-14 (-scaleTo:, -scale: float -> VFloat)
 *           2013-06-29 (-scaleTo: new - scale to saved scaleFactor)
 *           2012-08-13 (scaleFactor is VFloat now)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_TILESCROLLVIEW
#define CEN_H_TILESCROLLVIEW

#include <AppKit/AppKit.h>
#include <VHFShared/types.h>

@interface TileScrollView:NSScrollView
{
    id                      box;
    IBOutlet NSPopUpButton  *resPopupListButton;

    id		document;
    VFloat	oldScaleFactor;
}

/* instance methods */
- initWithFrame:(NSRect)theFrame;
- (void)setDocument:docu;

- (void)zoomIn:sender;
- (void)zoomOut:sender;
- (void)magnify:sender;
- (void)changeScale:sender;

- (void)magnifyRegion:(NSRect)region;
- (void)scaleTo:(VFloat)scaleFactor;
- (VFloat)scaleFactor;
- (void)tile;
- (void)setDocumentView:(NSView *)aView;

@end

@interface possibleDocument:NSObject
- (void)scale:(VFloat)x :(VFloat)y withCenter:(NSPoint)center;
- (void)setMagnify:(BOOL)flag;
@end

#endif // CEN_H_TILESCROLLVIEW
