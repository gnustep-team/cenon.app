/* DocWindow.h
 * Cenon document window class
 *
 * Copyright (C) 1995-2015 by Cenon GmbH
 * Author: Georg Fleischmann
 *
 * Created:  1995-12-02
 * Modified: 2013-07-04 (-windowDidBecomeKey: added)
 *           2010-04-19 (unfoldedHeight)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_DOCWINDOW
#define CEN_H_DOCWINDOW

#include <AppKit/AppKit.h>

/* notifications */
#define DocWindowDidChange	@"DocWindowDidChange"		// DataPanel needs update

@interface DocWindow:NSWindow
{
    id		wCoord;
    id		hCoord;
    id		xCoord;
    id		yCoord;
    id		unitPopup;          // mm, inch, pt
    id		tileScrollView;
    id		coordBox;

    id		document;           // the relating document
    int		unit;               // the unit
    NSPoint	refPoint;           // the refernce point for the coordinate display
    NSTimer	*timer;
#ifdef __APPLE__
    VFloat   unfoldedHeight;    // height of unfolded window, if folded
#endif
}

- document;
- (void)setDocument:docu;

#ifdef __APPLE__            // stuff to fold window into title bar
- (VFloat)unfoldedHeight;
- (void)setUnfoldedHeight:(VFloat)h;
- (BOOL)isFolded;
#endif

- (NSSize)coordBoxSize;		// return size of coord box

- (void)setUnit:sender;
- (int)unit;

- (float)convertToUnit:(float)value;

- (void)enableCoordDisplay:(BOOL)enable;
- (BOOL)hasCoordDisplay;
- (void)displayCoordinate:(NSPoint)p0 ref:(BOOL)ref;

/* delegate methods
 */
- (void)windowDidBecomeMain:(NSNotification *)notification;
- (void)windowDidBecomeKey:(NSNotification *)notification;
//- (void)windowDidResignMain:(NSNotification *)notification;
- (void)windowDidResignKey:(NSNotification *)notification;
//- (BOOL)windowShouldClose:(id)sender;

@end

#endif // CEN_H_DOCWINDOW
