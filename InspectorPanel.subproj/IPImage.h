/* IPImage.h
 * Image inspector
 *
 * Copyright (C) 1996-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  1998-03-23
 * modified: 2014-07-04 (IBOutlet NSPopUpButton  *compPopUp)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_IPIMAGE
#define CEN_H_IPIMAGE

#include <AppKit/AppKit.h>
#include "IPBasicLevel.h"

@interface IPImage:IPBasicLevel
{
    id	xField;
    id	yField;
    id	widthField;
    id	heightField;
    //id	reliefSwitch;
    //id	reliefPopUp;
    id	thumbSwitch;
    id	nameField;
    id	factorField;
    IBOutlet NSPopUpButton  *compPopUp;

    VGraphic	*graphic;	// the loaded graphic or the first of them if multiple
}

- (void)update:sender;

- (void)setX:sender;
- (void)setY:sender;
- (void)setWidth:sender;
- (void)setHeight:sender;
//- (void)setRelief:sender;
//- (void)setReliefType:sender;
- (void)setThumbnail:sender;
- (void)setName:sender;
- (void)setCompressionFactor:sender;
- (void)setCompressionType:sender;

- (void)displayWillEnd;

@end

#endif // CEN_H_IPIMAGE
