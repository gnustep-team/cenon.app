/* IPRectangle.h
 * Rectangle Inspector
 *
 * Copyright (C) 1996-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  1997-11-14
 * modified: 2002-07-20
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_IPRECTANGLE
#define CEN_H_IPRECTANGLE

#include <AppKit/AppKit.h>
#include "IPBasicLevel.h"

@interface IPRectangle:IPBasicLevel
{
    id	angleField;
    id	angleSlider;
    id	xField;
    id	yField;
    id	sizeWidthField;
    id	sizeHeightField;
    id	radiusField;
}

- (void)update:sender;

- (void)setAngle:sender;
- (void)setX:sender;
- (void)setY:sender;
- (void)setSizeWidth:sender;
- (void)setSizeHeight:sender;
- (void)setRadius:sender;

- (void)displayWillEnd;

@end

#endif // CEN_H_IPRECTANGLE
