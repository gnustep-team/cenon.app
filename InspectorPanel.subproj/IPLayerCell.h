/* IPLayerCell.h
 * part of IPAllLayers
 *
 * Copyright (C) 1996-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * Created:  2002-10-05 from LayerCell 1996-03-07
 * Modified: 2002-10-07
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_IPLAYERCELL
#define CEN_H_IPLAYERCELL

#include "../LayerObject.h"
#include "../MoveCell.h"

@interface IPLayerCell: MoveCell
{
    LayerObject	*layerObject;
}

- init;
- (void)setLayerObject:(LayerObject*)theObject;
- (LayerObject*)layerObject;
- (BOOL)dependant;
- (void)drawInteriorWithFrame:(NSRect)cellFrame inView:(NSView *)controlView;
- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aView untilMouseUp:(BOOL)_untilMouseUp;

/* methods implemented by the delegate (of the matrix)
 */
- (void)cellDidChangeSide:sender;

@end

#endif // CEN_H_IPLAYERCELL
