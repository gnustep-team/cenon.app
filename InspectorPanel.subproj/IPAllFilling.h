/* IPAllFilling.h
 * Fill Inspector for all objects
 *
 * Copyright (C) 2002-2014 by Cenon GmbH
 * Author:   Ilonka Fleischmann
 *
 * created:  2002-06-27
 * modified: 2014-07-14 (IBOutlet NSPopUpButton  *fillPopup)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.de
 * http://www.Cenon.de
 */

#ifndef CEN_H_IPALLFILLING
#define CEN_H_IPALLFILLING

#include <AppKit/AppKit.h>
#include "IPBasicLevel.h"

@interface IPAllFilling:IPBasicLevel
{
    id angleField;
    id angleSlider;
    id stepForm;
    id colorWell;
    id colorWellGraduated;
    IBOutlet NSPopUpButton  *fillPopup;
    id sliderBox;
    id radialCenterText;
    id angleButtonLeft;
    id angleButtonRight;
    id stepButtonLeft;
    id stepButtonRight;

    VGraphic	*graphic;	// the loaded graphic or the first of them if multiple
}

- (void)update:sender;

- (void)setAngle:(id)sender;
- (void)setStepWidth:sender;
- (void)setFillColor:(id)sender;
- (void)setEndColor:(id)sender;
- (void)setFillState:(id)sender;
- (void)setRadialCenter:(id)sender;

- (void)displayWillEnd;

@end

#endif // CEN_H_IPALLFILLING
