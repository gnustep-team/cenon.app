/* IPArc.h
 * Arc Inspector
 *
 * Copyright (C) 1995-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  1995-12-09
 * modified: 2003-06-05
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_IPALLARC
#define CEN_H_IPALLARC

#include <AppKit/AppKit.h>
#include "IPBasicLevel.h"

@interface IPArc:IPBasicLevel
{
    id	centerXField;
    id	centerYField;
    id	radiusField;
    id	begAngleField;
    id	begAngleSlider;
    id	angleField;
    id	angleSlider;
}

- (void)update:sender;

- (void)setCenterX:sender;
- (void)setCenterY:sender;
- (void)setRadius:sender;
- (void)setBegAngle:sender;
- (void)setAngle:sender;

@end

#endif // CEN_H_IPALLARC
