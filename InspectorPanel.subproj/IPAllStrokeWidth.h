/* IPAllStrokeWidth.h
 * Stroke Inspector for all graphic objects
 *
 * Copyright (C) 2002-2014 by Cenon GmbH
 * Author:   Ilonka Fleischmann
 *
 * created:  2002-06-27
 * modified: 2014-07-04 (IBOutlet NSPopUpButton  *strokePopup)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_IPALLStrokeWidth
#define CEN_H_IPALLStrokeWidth

#include <AppKit/AppKit.h>
#include "IPBasicLevel.h"

@interface IPAllStrokeWidth:IPBasicLevel
{
    int		stroked;
    IBOutlet NSPopUpButton  *strokePopup;
    id 		colorWell;
    id 		widthField;
    id 		widthSlider;
    id		widthButtonLeft;
    id		widthButtonRight;

    VGraphic	*graphic;
}

- (void)update:sender;

- (void)setColor:(id)sender;
- (void)setWidth:(id)sender;
- (void)setStrokeState:(id)sender;

- (void)displayWillEnd;

@end

#endif // CEN_H_IPALLStrokeWidth
