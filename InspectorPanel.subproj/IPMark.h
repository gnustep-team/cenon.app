/* IPMark.h
 * Mark Inspector
 *
 * Copyright (C) 1999-2014 by Cenon GmbH
 * Author:  Georg Fleischmann
 *
 * created:  1997-11-13
 * modified: 2011-05-30 (nameFIeld, -setName: is deprecated)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_IPMARK
#define CEN_H_IPMARK

#include <AppKit/AppKit.h>
#include "IPBasicLevel.h"

@interface IPMark:IPBasicLevel
{
    id	xField;
    id	yField;
    id	zSwitch;	// to enable/disable Z field
    id	zField;
    id	zLeftButton;
    id	zRightButton;
    id	nameField;  // DEPRECATED, use labelField
}

- (void)update:sender;

- (void)setPointX:sender;
- (void)setPointY:sender;
- (void)setPointZ:sender;
- (void)setName:sender;     // DEPRECATED, use -setLabel:

- (void)displayWillEnd;

@end

#endif // CEN_H_IPMARK
