/* IPAllLayers.h
 * Layer management Inspector for all objects
 *
 * Copyright (C) 2002-2016 by Cenon GmbH
 * Author:   Ilonka Fleischmann, Georg Fleischmann
 *
 * created:  2002-06-27
 * modified: 2016-12-24 (-editingChanged: new)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_IPALLLAYERS
#define CEN_H_IPALLLAYERS

#include <AppKit/AppKit.h>
#include "IPBasicLevel.h"
#include "../LayerObject.h"

@interface IPAllLayers:IPBasicLevel
{
    IBOutlet id	moveMatrix;
    IBOutlet id	nameField;

    id	docView;
    id	lastLayerList;
    int	moveCellCount;
}

- init;
- (LayerObject*)currentLayerObject;
- (void)update:sender;

- (IBAction)setName:sender;
- (IBAction)changeLayer:sender;
- (IBAction)newLayer:sender;
- (IBAction)removeLayer:sender;

- (BOOL)updateLayerLists;
- (void)displayChanged:sender;  // called from IPLayerCell to inform if display changed
- (void)editingChanged:sender;  // called from IPLayerCell to inform if editing changed

- (void)displayWillEnd;

@end

#endif // CEN_H_IPALLLAYERS
