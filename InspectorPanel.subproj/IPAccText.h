/* IPAccText.h
 * Accessory Text Inspector used for all Text objects
 *
 * Copyright (C) 2008 by Cenon GmbH
 * Author:   Ilonka Fleischmann
 *
 * created:  2008-03-13
 * modified: 2008-03-13
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_IPACCTEXT
#define CEN_H_IPACCTEXT

#include <AppKit/AppKit.h>
#include "IPBasicLevel.h"

@interface IPAccText:IPBasicLevel
{
    id fontSizeField;
    id lineHeightField;
    id fontButtonLeft;
    id fontButtonRight;
    id lineButtonLeft;
    id lineButtonRight;

    VGraphic	*graphic;	// the loaded graphic or the first of them if multiple
}

- (void)update:sender;

- (void)setFontSize:(id)sender;
- (void)setLineHeight:(id)sender;

- (void)displayWillEnd;

@end

#endif // CEN_H_IPACCTEXT
