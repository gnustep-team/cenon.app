/*
 * WorkingAreaPanel.h
 *
 * Copyright (C) 1996-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  2000-08-24
 * modified: 2008-07-19
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_WORKINGAREAPANEL
#define CEN_H_WORKINGAREAPANEL

#include <AppKit/AppKit.h>
//#include "functions.h"  // CenonUnit

@interface WorkingAreaPanel: NSPanel
{
    id          sizeMatrix;
    //CenonUnit   myUnit;
}

- (void)setWorkingArea:sender;
- (void)update:sender;

@end

#endif // CEN_H_WORKINGAREAPANEL
