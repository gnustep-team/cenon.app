/* TPScale.h
 * Transform panel for scaling objects
 *
 * Copyright (C) 1996-2014 by Cenon GmbH
 * Author: Georg Fleischmann
 *
 * Created:  1996-04-22
 * Modified: 2011-03-03
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include <AppKit/AppKit.h>
#include "TransformPanel.h"

@interface TPScale:TPBasicLevel
{
    id  scalePopup;         // relative, scale to size
    id	scaleXField;
    id	scaleYField;
    id	uniformScaleSwitch; // on = keep aspekt ratio
}

- (void)setHowToScale:sender;
- (void)setUniformScale:sender;
- (void)setScale:sender;

@end
