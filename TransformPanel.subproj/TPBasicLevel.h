/*
 * TPBasicLevel.h
 *
 * Copyright (C) 1996-2015 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * Created:  1996-03-03
 * Modified: 2015-07-25 (NSWindowDelegate added)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_TPBASICLEVEL
#define CEN_H_TPBASICLEVEL

#include <AppKit/AppKit.h>
#include <VHFShared/vhfCommonFunctions.h>
#include <VHFShared/types.h>
#include "../App.h"

@interface TPBasicLevel:NSPanel <NSWindowDelegate>
{
    id	window;		// the data panel
}

- init;
- (void)setWindow:win;
- view;
- window;
- (void)update:sender;

/* delegate methods
 */
- (void)displayWillEnd;

@end

#endif // CEN_H_TPBASICLEVEL
