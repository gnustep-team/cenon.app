/* HiddenArea.h
 * Object for calculation of hidden areas
 *
 * Copyright (C) 1999-2014 by Cenon GmbH
 * Author:   Ilonka Fleischmann
 *
 * created:  1998-03-26
 * modified: 2006-02-06
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_HIDDENAREA
#define CEN_H_HIDDENAREA

@interface HiddenArea:NSObject

/* HiddenArea methods
 */
- (void)removeHiddenAreas:(NSMutableArray*)list;
- (void)uniteAreas:(NSMutableArray*)list;
- (void)removeSingleGraphicsInList:(NSMutableArray*)list :(NSRect)rect;
- (BOOL)removeGraphics:(NSMutableArray*)list inside:(id)graphic;
- (BOOL)removeGraphics:(NSMutableArray*)list outside:(id)graphic;
- (BOOL)removePartsOf:(VGraphic**)curG hiddenBy:(id)og;

@end

#endif // CEN_H_HIDDENAREA
