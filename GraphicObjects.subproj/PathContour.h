/* PathContour.h
 * Object to build contour of paths using a raster algorithm (scan)
 *
 * Copyright (C) 2000-2014 by Cenon GmbH
 * Author:   Ilonka Fleischmann
 *
 * created:  2000-02-23
 * modified: 2006-02-21
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_PATHCONTOUR
#define CEN_H_PATHCONTOUR

#include "VPath.h"
#include "VImage.h"

@interface PathContour:NSObject

/* PathContour methods
 */
-(VPath*)contourPath:(VPath*)oPath width:(float)width;
-(VPath*)contourImage:(VImage*)image width:(float)width;
-(VPath*)contourList:(NSArray*)oList width:(float)width;

@end

#endif // CEN_H_PATHCONTOUR
