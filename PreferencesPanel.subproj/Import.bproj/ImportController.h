/* ImportController.h
 * Preferences module for Cenon imports
 *
 * Copyright (C) 1996-2015 by Cenon GmbH
 * Author: Georg Fleischmann
 *
 * Created:  1999-03-15
 * Modified: 2015-09-02 (IBOutlet, IBAction added)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include <AppKit/AppKit.h>
#include "../PreferencesMethods.h"

/* indexes of switchMatrix */
typedef enum
{
    SWITCH_COLORTOLAYER =  0,
    SWITCH_FILLOBJECTS  =  1,
    SWITCH_MOVETOORIGIN =  2
} SwitchMatrix;

@interface ImportController:NSObject <PreferencesMethods>
{
    IBOutlet id         box;

    IBOutlet NSMatrix   *switchMatrix;

    IBOutlet id         hpglPopup;
    IBOutlet id         gerberPopup;
    IBOutlet id         dxfResField;
    IBOutlet id         psPreferArcsSwitch;
    IBOutlet id         psFlattenTextSwitch;
    IBOutlet id         dinPopup;
}

- (IBAction)set:sender;

@end
