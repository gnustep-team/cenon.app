/* PreferencesPanel.h
 * Control class of preferences panel
 *
 * Copyright (C) 1995-2015 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * Created:  1999-03-15
 * Modified: 2014-07-06 (IBOutlet + NSBox for moduleView)
 *           2011-09-13 (-update: new)
 *           2003-07-22
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include <AppKit/AppKit.h>

#define PP_IMPORT	0
#define PP_CONTROL	1
#define PP_MISC		3
#define PP_DEFAULT	99

#define BUTTONLEFT	0
#define BUTTONRIGHT	1
#define BUTTONUP	2
#define BUTTONDOWN	3

@interface PreferencesPanel:NSPanel <NSWindowDelegate>  // compiles >= OS X 10.6
{
    IBOutlet id     iconMatrix;     // the matrix of icons at the top of the panel
    IBOutlet NSBox  *moduleView;    // the view where the modules are placed

    NSMutableArray	*modules;       // the loaded modules
}

- init;
- (void)loadModules;
- (void)addBundleWithPath:(NSString*)path;

- (void)setModule:sender;
- (void)setModuleAt:(int)level orderFront:(BOOL)orderFront;
- (void)update:sender;

@end
