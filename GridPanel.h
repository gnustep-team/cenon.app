/* GridPanel.m
 * Panel for setting up the grid
 *
 * Copyright (C) 1997-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  2000-08-24
 * modified: 2002-07-07
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include <AppKit/AppKit.h>

@interface GridPanel: NSPanel
{
    id	gridField;
    id	unitPopUp;
}

//+ (void)runGridPanel:sender;
//+ (id)sharedGridPanel;

- (void)setGrid:sender;

@end
