/* TilePanel.h
 * Panel for batch production
 *
 * Copyright (C) 1996-2014 by Cenon GmbH
 * Author:   Georg Fleischmann
 *
 * created:  2000-08-08
 * modified: 2014-10-21 (xDirButton, yDirButton, -setXYDirections: new)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#ifndef CEN_H_TILEPANEL
#define CEN_H_TILEPANEL

#include <AppKit/AppKit.h>

@interface TilePanel: NSPanel
{
    id	distPopup;
    id	distanceMatrix;
    id	limitsPopUp;
    id	limitsMatrix;
    id	originSwitch;
    IBOutlet NSButton  *xDirButton;
    IBOutlet NSButton  *yDirButton;
}

- (BOOL)useAbsoluteDistance;
- (NSPoint)distance;		// distance values in panel fields (either relative or absolute)
- (NSPoint)relativeDistance;	// relative distance (between parts)
- (NSPoint)xyDirections;
- (BOOL)limitSize;
- (NSPoint)limits;
- (BOOL)mustMoveMasterToOrigin;

- (void)updatePanel:sender;
- (void)setDistancePopup:sender;
- (IBAction)setXYDirections:(id)sender;
- (void)set:sender;
- (void)buildCopies:sender;
- (void)removeTiles:sender;

@end

#endif // CEN_H_TILEPANEL
