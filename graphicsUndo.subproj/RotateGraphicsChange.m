/* RotateGraphicsChange.m
 *
 * Copyright (C) 1993-2014 by Cenon GmbH
 * Authors:  Georg Fleischmann
 *
 * created:  1993
 * modified: 2002-07-15
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the Cenon Public License as
 * published by Cenon GmbH. Among other things, the
 * License requires that the copyright notices and this notice
 * be preserved on all copies.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Cenon Public License for more details.
 *
 * You should have received a copy of the Cenon Public License along
 * with this program; see the file LICENSE. If not, write to Cenon.
 *
 * Cenon GmbH, Schwabstr. 45, 72108 Rottenburg a.N., Germany
 * eMail: info@Cenon.com
 * http://www.Cenon.de
 */

#include "undo.h"

@interface RotateGraphicsChange(PrivateMethods)

- (BOOL)subsumeIdenticalChange:change;

@end

@implementation RotateGraphicsChange

- initGraphicView:aGraphicView angle:(float)a
{
    [super initGraphicView:aGraphicView];
    useGraphicOrigin = YES;
    absolute = YES;
    redoAngle = a;
    undoAngle = -a;
    return self;
}

- initGraphicView:aGraphicView angle:(float)a center:(NSPoint)p
{
    [super initGraphicView:aGraphicView];
    useGraphicOrigin = NO;
    absolute = NO;
    center = p;
    redoAngle = a;
    undoAngle = -a;
    return self;
}

- (NSString *)changeName
{
    return ROTATE_OP;
}

- (Class)changeDetailClass
{
    return [RotateChangeDetail class];
}

- (BOOL)useGraphicOrigin
{
    return useGraphicOrigin;
}

- (float)undoAngle
{
    return undoAngle;
}

- (float)redoAngle
{
    return redoAngle;
}

- (NSPoint)center
{
    return center;
}

- (BOOL)subsumeIdenticalChange:change
{   RotateGraphicsChange	*rotateChange = (RotateGraphicsChange *)change;

    if (absolute)
    {
        undoAngle = rotateChange->undoAngle;
        redoAngle = rotateChange->redoAngle;
    }
    else
    {
        undoAngle += rotateChange->undoAngle;
        redoAngle += rotateChange->redoAngle;
    }
    return YES;
}

@end
